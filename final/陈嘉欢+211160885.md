
# Project Name: light-weighted (istio-less) traffic control for inference

### Scheme Description

In the AI inference scenarios, it usually involves multi-model services (loading different versions of AI models and providing inference services to users). For scenarios where multiple models coexist, it is usually necessary to do some refined routing for user inference requests.
	
Currently, some open source solutions such like kfserving usually rely on the heavier istio to do traffic forwarding, similar as the current implementation of kubedl.
	
But in fact istio is a very heavy dependency, what we actually need in inference scenario is only part of its gateway function，and if we want to do some customized serving-specific optimizations，the cost of customed maintenance for these external projects is high.
	
So we plan to build a light-weighted (istio-less) service-level traffic control for kubedl, not only for just simplifying the reliance of traffic management, but also for better optimize scenarios such as model canary release, multi-stage inferencing, and batching in the future.

### Time Planning

* 2021/07/01 - 2021/08/15 familiar myself with k8s/kubedl related technology stacks, investigate the open-source implementation of traffic control for inference, output design doc and submit it to the kubedl community
* 2021/08/16 - 2021/09/30 starting coding


### Project Output

##### first stage

In the first stage, our initial plan was "familiar myself with k8s/kubedl related technology stacks, investigate the open-source implementation of traffic control for inference, output design doc and submit it to the kubedl community". During this time, I studied related technology stacks, and took a research on the open source service gateways and inference frameworks, such as istio, nginx-ingress-gateway, kfserving and etc, to learn how they do canary releases and traffic control. After some research, I found that most of the open source gateways are very rich in functions, but they are not specifically designed for the inference scenario, so using them directly is either too heavy or difficult to customize. In the end, we decided to build a light-weighted gateway ourselves with Caddy. Caddy is a powerful, extensible platform to serve our services. At the end of this stage, I submitted the proprosal to the community and was accepted later.

##### second stage

The goal at this stage is very clear, that is landing the code. In order to couple the realization, I focused on writing the light-weighted(istio-less) ingress gateway first, and then combined kubedl with the function of gateway. I spent most of my time building this gateway with Caddy and testing its functions. After implementing the function of simple traffic control, it becomes easier to modify the code related to kubedl serving. I just modified the code related to the istio virtualservice, created our istio-less gateway pod and the corresponding ingress instead. At the end of this stage, I also submitted the corresponding implementation to the community, and the PR is still under the code review.

### Scheme Progress 

##### first stage

All of my investigation is in [PR163](https://github.com/kubedl-io/kubedl/pull/163)

##### second stage

All of my implementation is in [PR184](https://github.com/kubedl-io/kubedl/pull/184)


### Summary and Experience 

In general, the experience in  summer 2021 is very rewarding. I have come into contact with areas that I have never touched before: inference. l researched some open source implementation in the industry and gave a proposal after thinking about my topic. After the proposal was recognized by the community, I was really happy. Then I entered the code stage, I realized the very basic version after a lot of debugging and problem solving. I hope that in the future, this optimization can really land in the community and have more impact, I think this is probably the meaning of open source.

##### Development Quality

I take this project very seriously all the time, and always try my best to complete this project. Although it was delayed for a while, but fortunately, a very basic version was realized in the end, and I hope this feature will be accepted to the community soon.

##### Communication and Feedback with Mentor

My mentor introduced me to the detailed background at the beginning of the project, and we fully discussed the project during the proposal stage, which helped me realize this project greatly. So I am really thankful for my mentor.
